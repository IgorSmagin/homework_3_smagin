package test.main_test;

/**
 * Created by klient on 03.10.2016.
 */
import com.company.main.Main;
import org.junit.Test;
import static org.junit.Assert.*;
public class Main_test {
    @Test
    public void TestMinOfThree() {

        assertEquals(3, Main.minOfThree(3, 5, 8));
    }

    @Test
    public void Testfactorial() {

        assertEquals(6, Main.factorial(3));
    }

    @Test
    public void TestcompareString() {

        assertFalse(Main.compareString("nyaссс","nya"));
    }

    @Test
    public void TestconcatenateString() {

        assertEquals("nya1nya2", Main.concatenateString("nya1","nya2"));
    }

    @Test
    public void TestfindMinInArrayg() {
        int arr[]= {5,4,7,6};

        assertEquals(4, Main.findMinInArray(arr));
    }

    @Test
    public void TestsortArray() {
        int arr1[]= {1,2,3,4}, arr2[]={3,2,1,4};


        assertEquals(arr1, Main.sortArray(arr2));
    }

    @Test
    public void TestsumOfElementsUnderMainDiagonal() {
        int arr[][]={{1,2},{3,4}};

        assertEquals(3, Main.sumOfElementsUnderMainDiagonal(arr));
    }

}

